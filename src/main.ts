import App from "./App.vue";
import { createApp, provide, h } from "vue";
import { createPinia } from "pinia";
import { DefaultApolloClient } from "@vue/apollo-composable";
import { apolloClient } from "@/config/apollo_client";
import { registerPlugins } from "@/plugins";

const pinia = createPinia();

const app = createApp({
  setup() {
    provide(DefaultApolloClient, apolloClient);
  },
  render: () => h(App),
});

registerPlugins(app);

app.use(pinia);
app.mount("#app");
