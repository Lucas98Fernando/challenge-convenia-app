import gql from "graphql-tag";

export const CONVERSIONS_QUERY = gql`
  query Conversions {
    currencyConversion(baseCurrency: "BRL", convertCurrencies: ["USD", "EUR"]) {
      baseCurrencyInfo {
        code
        symbol
      }
      conversions {
        rate
        currencyInfo {
          code
          symbol
        }
        timestamp
      }
    }
  }
`;
