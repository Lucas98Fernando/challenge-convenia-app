export function isMobileDevice() {
  const devices = [
    /Android/i,
    /webOS/i,
    /iPhone/i,
    /iPad/i,
    /iPod/i,
    /BlackBerry/i,
    /Windows Phone/i,
  ];

  return devices.some((item) => navigator.userAgent.match(item));
}
