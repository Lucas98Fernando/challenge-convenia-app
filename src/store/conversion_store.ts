import { defineStore } from "pinia";
import { CONVERSIONS_QUERY } from "@/graphql/conversion_query";
import { CurrencyConversion, IConversionState } from "@/types/conversion_types";
import {
  getValueToPayByPerson,
  getTipCalculate,
  getCurrencyIcon,
  getCurrencyToBRL,
  getCurrencyRateByName,
  getTotalToPay,
} from "@/store/helpers";
import { useQuery } from "@vue/apollo-composable";
import toast from "@/components/toast/toast.vue";

export const useConversionStore = defineStore("conversion", {
  state: (): IConversionState => ({
    valueToConvert: "0",
    tip: "10",
    peopleAmount: "2",
    isCurrencyUSD: false,
    quotationUSD: 0,
    quotationEUR: 0,
  }),
  getters: {
    getCurrencyActive: (state) => getCurrencyIcon(state),
    conversionDetailsModel: (state) => [
      {
        label: "Conta",
        value: state.valueToConvert,
        symbol: getCurrencyIcon(state),
      },
      {
        label: "Gorjeta",
        value: getTipCalculate(state).toFixed(2),
        symbol: getCurrencyIcon(state),
      },
      {
        label: "Total",
        value: getTotalToPay(state).toFixed(2),
        symbol: getCurrencyIcon(state),
      },
      {
        label: "por Pessoa",
        value: getValueToPayByPerson(state).toFixed(2),
        symbol: getCurrencyIcon(state),
      },
      {
        label: "em R$",
        value: getCurrencyToBRL(state).toFixed(2),
        symbol: "brl",
      },
    ],
  },
  actions: {
    fetchQuotations() {
      const { onResult, onError } = useQuery(CONVERSIONS_QUERY);

      onResult((queryResult) => {
        const data = queryResult.data
          .currencyConversion as unknown as CurrencyConversion;

        this.quotationUSD = getCurrencyRateByName(data.conversions, "USD");
        this.quotationEUR = getCurrencyRateByName(data.conversions, "EUR");
      });

      onError(() => toast.emits?.runToast());
    },
  },
});
