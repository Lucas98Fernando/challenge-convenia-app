import { ConversionInfo, IConversionState } from "@/types/conversion_types";

export function getValueToPayByPerson(state: IConversionState) {
  return Number(Number(state.valueToConvert) / Number(state.peopleAmount));
}

export function getTipCalculate(state: IConversionState) {
  return Number(state.valueToConvert) * (Number(state.tip) / 100);
}

export function getTotalToPay(state: IConversionState) {
  return Number(state.valueToConvert) + getTipCalculate(state);
}

export function getCurrencyIcon(state: IConversionState) {
  return state.isCurrencyUSD ? "usd" : "eur";
}

export function getCurrencyToBRL(state: IConversionState) {
  const currency = state.isCurrencyUSD
    ? state.quotationUSD
    : state.quotationEUR;

  return getTotalToPay(state) * currency;
}

export function getCurrencyRateByName(
  data: ConversionInfo[],
  currencyName: string
) {
  return (
    data.find((el) => el.currencyInfo.code === currencyName.toUpperCase())
      ?.rate || 0
  );
}
