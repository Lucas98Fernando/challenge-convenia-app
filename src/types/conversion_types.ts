export type CurrencyConversion = {
  baseCurrencyInfo: CurrencyInfo;
  conversions: ConversionInfo[];
};

export type CurrencyInfo = {
  code: string;
  symbol: string;
};

export type ConversionInfo = {
  rate: number;
  timestamp: number;
  currencyInfo: CurrencyInfo;
};

export type IConversionState = {
  valueToConvert: string;
  tip: string;
  peopleAmount: string;
  isCurrencyUSD: boolean;
  quotationUSD: number;
  quotationEUR: number;
};
